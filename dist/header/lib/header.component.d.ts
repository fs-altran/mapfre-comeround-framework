import { OnInit, ElementRef, Renderer2 } from '@angular/core';
import { MfcHeaderProperties } from './header.interface';
export declare class HeaderComponent implements OnInit {
    private renderer;
    mfcProperties: MfcHeaderProperties;
    private title;
    private isLogoLink;
    logoUpperBarImageAlt: any;
    contactList: ElementRef;
    constructor(renderer: Renderer2);
    ngOnInit(): void;
    initialize(): void;
    _resize(): void;
    updateTitle(newValue: any): void;
    watchRnr(): void;
    updateContactPhone(rnr: any): void;
    popup(url: any): void;
    checkStyle(title: any): void;
    openIconLinks(type: any, link: any): void;
    manageLink(type: any, target: any): void;
}
