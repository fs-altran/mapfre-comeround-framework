export interface MfcHeaderProperties {
    name?: string;
    logo?: string;
    manageLinkLogo?: {
        title?: string;
        type?: string;
        target?: string;
    };
    logoUpperBar?: string;
    title?: string;
    hideTitle?: boolean;
    titleStates?: {
        states?: string[];
        titles?: string[];
    };
    isFixed?: boolean;
    subtitle?: string;
    contactPhone?: string;
    contactButtonLabel?: string;
    contactButton?: string;
    contactInfo?: {
        title?: string;
        open?: string;
        url?: string;
    };
    contactUs?: string;
    contactLink?: string;
    isLogoLink?: string;
    iconLinks?: IconLinksModel[];
    logoPosition?: string;
    upperBar?: boolean;
    richedContent?: any;
    classname?: string;
    checkStyle?: any;
    logoImageAlt?: any;
}
export interface IconLinksModel {
    icon?: string;
    title?: string;
    open?: string;
    url?: string;
}
