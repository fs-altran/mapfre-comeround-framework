/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { isObject } from 'util';
export class FcsTools {
    /**
     * @param {?} object
     * @param {?} name
     * @param {?} deepLevel
     * @param {?} ignoreLevels
     * @param {?=} getParent
     * @return {?}
     */
    static findObjectInObject(object, name, deepLevel, ignoreLevels, getParent) {
        // tslint:disable-next-line:variable-name
        /** @type {?} */
        const _self = FcsTools;
        /** @type {?} */
        const item = {};
        item.value = undefined;
        if (!ignoreLevels) {
            ignoreLevels = 0;
        }
        if (deepLevel > 0) {
            Object.keys[object].forEach((/**
             * @param {?} key
             * @return {?}
             */
            (key) => {
                /** @type {?} */
                const value = object[key];
                if (key === name && 0 >= ignoreLevels) {
                    item.value = value;
                    deepLevel = 0;
                }
                else {
                    if (!item.value && item.value !== '' && item.value !== false && deepLevel > 0 && isObject(value)) {
                        item.value = _self.findObjectInObject(value, name, deepLevel - 1, ignoreLevels - 1).value;
                        if ((item.value || item.value === '' || item.value === false) && getParent) {
                            item.parent = key;
                        }
                    }
                }
            }));
        }
        return item;
    }
    /**
     * @param {?} param
     * @return {?}
     */
    static isEmpty(param) {
        /** @type {?} */
        const hasOwnProperty = Object.prototype.hasOwnProperty;
        // null y undefined se consideran vacíos
        if (param == null) {
            return true;
        }
        // Se asume que no está vacío si el parametro tiene una propiedad length y es mayor que 0
        if (param.length > 0) {
            return false;
        }
        if (param.length === 0) {
            return true;
        }
        // En este punto, si el parámetro NO es un objeto, se considera vacío
        // if (typeof param !== "object") return true;
        if (!isObject(param)) {
            return true;
        }
        // Comprueba si el objeto tiene propiedades "propias", si no tiene ninguna, se considera vacío
        for (const key in param) {
            if (hasOwnProperty.call(param, key)) {
                return false;
            }
        }
        return true;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmNzVG9vbHNTZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vaGVhZGVyLyIsInNvdXJjZXMiOlsidXRpbHMvZmNzVG9vbHNTZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRWhDLE1BQU0sT0FBTyxRQUFROzs7Ozs7Ozs7SUFFbkIsTUFBTSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxTQUFlOzs7Y0FFeEUsS0FBSyxHQUFHLFFBQVE7O2NBQ2hCLElBQUksR0FBUSxFQUFFO1FBQ3BCLElBQUksQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDakIsWUFBWSxHQUFHLENBQUMsQ0FBQztTQUNsQjtRQUNELElBQUksU0FBUyxHQUFHLENBQUMsRUFBRTtZQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFOztzQkFDNUIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ3pCLElBQUksR0FBRyxLQUFLLElBQUksSUFBSSxDQUFDLElBQUksWUFBWSxFQUFFO29CQUNyQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztvQkFDbkIsU0FBUyxHQUFHLENBQUMsQ0FBQztpQkFDZjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLEtBQUssSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDaEcsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxTQUFTLEdBQUcsQ0FBQyxFQUFFLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7d0JBQzFGLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLElBQUksU0FBUyxFQUFFOzRCQUMxRSxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQzt5QkFDbkI7cUJBQ0Y7aUJBQ0Y7WUFDSCxDQUFDLEVBQUMsQ0FBQztTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSzs7Y0FDWixjQUFjLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxjQUFjO1FBQ3RELHdDQUF3QztRQUN4QyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztTQUFFO1FBRW5DLHlGQUF5RjtRQUN6RixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQUUsT0FBTyxLQUFLLENBQUM7U0FBRTtRQUN2QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQUUsT0FBTyxJQUFJLENBQUM7U0FBRTtRQUV4QyxxRUFBcUU7UUFDckUsOENBQThDO1FBQzlDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFBRSxPQUFPLElBQUksQ0FBQztTQUFFO1FBRXRDLDhGQUE4RjtRQUM5RixLQUFLLE1BQU0sR0FBRyxJQUFJLEtBQUssRUFBRTtZQUN2QixJQUFJLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxFQUFFO2dCQUFFLE9BQU8sS0FBSyxDQUFDO2FBQUU7U0FDdkQ7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGlzT2JqZWN0IH0gZnJvbSAndXRpbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRmNzVG9vbHMge1xyXG5cclxuICBzdGF0aWMgZmluZE9iamVjdEluT2JqZWN0KG9iamVjdCwgbmFtZSwgZGVlcExldmVsLCBpZ25vcmVMZXZlbHMsIGdldFBhcmVudD86IGFueSkge1xyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnZhcmlhYmxlLW5hbWVcclxuICAgIGNvbnN0IF9zZWxmID0gRmNzVG9vbHM7XHJcbiAgICBjb25zdCBpdGVtOiBhbnkgPSB7fTtcclxuICAgIGl0ZW0udmFsdWUgPSB1bmRlZmluZWQ7XHJcbiAgICBpZiAoIWlnbm9yZUxldmVscykge1xyXG4gICAgICBpZ25vcmVMZXZlbHMgPSAwO1xyXG4gICAgfVxyXG4gICAgaWYgKGRlZXBMZXZlbCA+IDApIHtcclxuICAgICAgT2JqZWN0LmtleXNbb2JqZWN0XS5mb3JFYWNoKChrZXkpID0+IHtcclxuICAgICAgICBjb25zdCB2YWx1ZSA9IG9iamVjdFtrZXldO1xyXG4gICAgICAgIGlmIChrZXkgPT09IG5hbWUgJiYgMCA+PSBpZ25vcmVMZXZlbHMpIHtcclxuICAgICAgICAgIGl0ZW0udmFsdWUgPSB2YWx1ZTtcclxuICAgICAgICAgIGRlZXBMZXZlbCA9IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmICghaXRlbS52YWx1ZSAmJiBpdGVtLnZhbHVlICE9PSAnJyAmJiBpdGVtLnZhbHVlICE9PSBmYWxzZSAmJiBkZWVwTGV2ZWwgPiAwICYmIGlzT2JqZWN0KHZhbHVlKSkge1xyXG4gICAgICAgICAgICBpdGVtLnZhbHVlID0gX3NlbGYuZmluZE9iamVjdEluT2JqZWN0KHZhbHVlLCBuYW1lLCBkZWVwTGV2ZWwgLSAxLCBpZ25vcmVMZXZlbHMgLSAxKS52YWx1ZTtcclxuICAgICAgICAgICAgaWYgKChpdGVtLnZhbHVlIHx8IGl0ZW0udmFsdWUgPT09ICcnIHx8IGl0ZW0udmFsdWUgPT09IGZhbHNlKSAmJiBnZXRQYXJlbnQpIHtcclxuICAgICAgICAgICAgICBpdGVtLnBhcmVudCA9IGtleTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gaXRlbTtcclxuICB9XHJcblxyXG4gIHN0YXRpYyBpc0VtcHR5KHBhcmFtKSB7XHJcbiAgICBjb25zdCBoYXNPd25Qcm9wZXJ0eSA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHk7XHJcbiAgICAvLyBudWxsIHkgdW5kZWZpbmVkIHNlIGNvbnNpZGVyYW4gdmFjw61vc1xyXG4gICAgaWYgKHBhcmFtID09IG51bGwpIHsgcmV0dXJuIHRydWU7IH1cclxuXHJcbiAgICAvLyBTZSBhc3VtZSBxdWUgbm8gZXN0w6EgdmFjw61vIHNpIGVsIHBhcmFtZXRybyB0aWVuZSB1bmEgcHJvcGllZGFkIGxlbmd0aCB5IGVzIG1heW9yIHF1ZSAwXHJcbiAgICBpZiAocGFyYW0ubGVuZ3RoID4gMCkgeyByZXR1cm4gZmFsc2U7IH1cclxuICAgIGlmIChwYXJhbS5sZW5ndGggPT09IDApIHsgcmV0dXJuIHRydWU7IH1cclxuXHJcbiAgICAvLyBFbiBlc3RlIHB1bnRvLCBzaSBlbCBwYXLDoW1ldHJvIE5PIGVzIHVuIG9iamV0bywgc2UgY29uc2lkZXJhIHZhY8Otb1xyXG4gICAgLy8gaWYgKHR5cGVvZiBwYXJhbSAhPT0gXCJvYmplY3RcIikgcmV0dXJuIHRydWU7XHJcbiAgICBpZiAoIWlzT2JqZWN0KHBhcmFtKSkgeyByZXR1cm4gdHJ1ZTsgfVxyXG5cclxuICAgIC8vIENvbXBydWViYSBzaSBlbCBvYmpldG8gdGllbmUgcHJvcGllZGFkZXMgXCJwcm9waWFzXCIsIHNpIG5vIHRpZW5lIG5pbmd1bmEsIHNlIGNvbnNpZGVyYSB2YWPDrW9cclxuICAgIGZvciAoY29uc3Qga2V5IGluIHBhcmFtKSB7XHJcbiAgICAgIGlmIChoYXNPd25Qcm9wZXJ0eS5jYWxsKHBhcmFtLCBrZXkpKSB7IHJldHVybiBmYWxzZTsgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxufVxyXG4iXX0=