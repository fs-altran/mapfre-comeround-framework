/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of header
 */
export { HeaderService } from './lib/header.service';
export { HeaderComponent } from './lib/header.component';
export { HeaderModule } from './lib/header.module';
export {} from './lib/header.interface';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2hlYWRlci8iLCJzb3VyY2VzIjpbInB1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUlBLDhCQUFjLHNCQUFzQixDQUFDO0FBQ3JDLGdDQUFjLHdCQUF3QixDQUFDO0FBQ3ZDLDZCQUFjLHFCQUFxQixDQUFDO0FBQ3BDLGVBQWMsd0JBQXdCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIGhlYWRlclxuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL2hlYWRlci5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2hlYWRlci5jb21wb25lbnQnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvaGVhZGVyLm1vZHVsZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9oZWFkZXIuaW50ZXJmYWNlJztcbiJdfQ==