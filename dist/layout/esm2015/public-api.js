/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of layout
 */
export { LayoutService } from './lib/layout.service';
export { LayoutComponent } from './lib/layout.component';
export { LayoutModule } from './lib/layout.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2xheW91dC8iLCJzb3VyY2VzIjpbInB1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUlBLDhCQUFjLHNCQUFzQixDQUFDO0FBQ3JDLGdDQUFjLHdCQUF3QixDQUFDO0FBQ3ZDLDZCQUFjLHFCQUFxQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBsYXlvdXRcbiAqL1xuXG5leHBvcnQgKiBmcm9tICcuL2xpYi9sYXlvdXQuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9sYXlvdXQuY29tcG9uZW50JztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2xheW91dC5tb2R1bGUnO1xuIl19