/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function MfcCardProperties() { }
if (false) {
    /** @type {?|undefined} */
    MfcCardProperties.prototype.title;
    /** @type {?|undefined} */
    MfcCardProperties.prototype.subtitle;
    /** @type {?|undefined} */
    MfcCardProperties.prototype.icon;
    /** @type {?|undefined} */
    MfcCardProperties.prototype.imagePosition;
    /** @type {?|undefined} */
    MfcCardProperties.prototype.visibleOnMobile;
    /** @type {?|undefined} */
    MfcCardProperties.prototype.helpField;
    /** @type {?|undefined} */
    MfcCardProperties.prototype.content;
    /** @type {?|undefined} */
    MfcCardProperties.prototype.classname;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC5pbnRlcmZhY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jYXJkLyIsInNvdXJjZXMiOlsibGliL2NhcmQuaW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQSx1Q0FtQkc7OztJQWxCQyxrQ0FBZTs7SUFDZixxQ0FBa0I7O0lBQ2xCLGlDQUFjOztJQUNkLDBDQUF1Qjs7SUFDdkIsNENBQTBCOztJQUMxQixzQ0FLRTs7SUFDRixvQ0FLRTs7SUFDRixzQ0FBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIE1mY0NhcmRQcm9wZXJ0aWVzIHtcclxuICAgIHRpdGxlPzogc3RyaW5nO1xyXG4gICAgc3VidGl0bGU/OiBzdHJpbmc7XHJcbiAgICBpY29uPzogc3RyaW5nO1xyXG4gICAgaW1hZ2VQb3NpdGlvbj86IHN0cmluZztcclxuICAgIHZpc2libGVPbk1vYmlsZT86IGJvb2xlYW47XHJcbiAgICBoZWxwRmllbGQ/OiB7XHJcbiAgICAgIHRvb2x0aXA/OiBzdHJpbmc7XHJcbiAgICAgIHRvb2x0aXBVcmw/OiBzdHJpbmc7XHJcbiAgICAgIHRvb2xUaXBTZWxlY3Q/OiBzdHJpbmc7XHJcbiAgICAgIG1vZGU/OiBzdHJpbmc7XHJcbiAgICB9O1xyXG4gICAgY29udGVudD86IHtcclxuICAgICAgZGVzY3JpcHRpb24/OiBzdHJpbmc7XHJcbiAgICAgIGltZ0Rlc2t0b3BVcmw/OiBzdHJpbmc7XHJcbiAgICAgIGltZ01vYmlsZVVybD86IHN0cmluZztcclxuICAgICAgZGVzY3JpcHRpb25Qb3N0Pzogc3RyaW5nO1xyXG4gICAgfTtcclxuICAgIGNsYXNzbmFtZT86IHN0cmluZztcclxuICB9XHJcbiJdfQ==