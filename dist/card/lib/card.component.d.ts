import { OnInit } from '@angular/core';
import { MfcCardProperties } from './card.interface';
export declare class CardComponent implements OnInit {
    mfcProperties: MfcCardProperties;
    richedTextDescription: any;
    richedTextDescriptionPost: any;
    mobile: any;
    visibleOnMobile: any;
    constructor();
    ngOnInit(): void;
    initialize(): void;
}
