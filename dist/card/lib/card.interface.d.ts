export interface MfcCardProperties {
    title?: string;
    subtitle?: string;
    icon?: string;
    imagePosition?: string;
    visibleOnMobile?: boolean;
    helpField?: {
        tooltip?: string;
        tooltipUrl?: string;
        toolTipSelect?: string;
        mode?: string;
    };
    content?: {
        description?: string;
        imgDesktopUrl?: string;
        imgMobileUrl?: string;
        descriptionPost?: string;
    };
    classname?: string;
}
