import { OnInit } from '@angular/core';
interface MfcRichedContentProperties {
    content?: string;
    visibleOnMobile?: boolean;
    name?: string;
    classname?: string;
}
export declare class MfcRichedContentComponent implements OnInit {
    mfcProperties: MfcRichedContentProperties;
    parentName: string;
    componentName: string;
    mobile: any;
    constructor();
    ngOnInit(): void;
    initialize(): void;
    outcome(event: any): void;
}
export {};
