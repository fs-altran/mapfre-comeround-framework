/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of card
 */
export { CardService } from './lib/card.service';
export { CardComponent } from './lib/card.component';
export { CardModule } from './lib/card.module';
export {} from './lib/card.interface';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NhcmQvIiwic291cmNlcyI6WyJwdWJsaWMtYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSw0QkFBYyxvQkFBb0IsQ0FBQztBQUNuQyw4QkFBYyxzQkFBc0IsQ0FBQztBQUNyQywyQkFBYyxtQkFBbUIsQ0FBQztBQUNsQyxlQUFjLHNCQUFzQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBjYXJkXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvY2FyZC5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NhcmQuY29tcG9uZW50JztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NhcmQubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2NhcmQuaW50ZXJmYWNlJztcbiJdfQ==