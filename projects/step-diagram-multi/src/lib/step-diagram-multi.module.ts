import { NgModule } from '@angular/core';
import { StepDiagramMultiComponent } from './step-diagram-multi.component';

@NgModule({
  declarations: [StepDiagramMultiComponent],
  imports: [
  ],
  exports: [StepDiagramMultiComponent]
})
export class StepDiagramMultiModule { }
