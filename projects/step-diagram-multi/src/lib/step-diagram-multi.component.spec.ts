import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepDiagramMultiComponent } from './step-diagram-multi.component';

describe('StepDiagramMultiComponent', () => {
  let component: StepDiagramMultiComponent;
  let fixture: ComponentFixture<StepDiagramMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepDiagramMultiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepDiagramMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
