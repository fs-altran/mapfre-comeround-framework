import { TestBed } from '@angular/core/testing';

import { StepDiagramMultiService } from './step-diagram-multi.service';

describe('StepDiagramMultiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StepDiagramMultiService = TestBed.get(StepDiagramMultiService);
    expect(service).toBeTruthy();
  });
});
