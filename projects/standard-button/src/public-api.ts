/*
 * Public API Surface of standard-button
 */

export * from './lib/standard-button.service';
export * from './lib/standard-button.component';
export * from './lib/standard-button.module';
