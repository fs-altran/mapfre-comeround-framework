import { TestBed } from '@angular/core/testing';

import { StandardButtonService } from './standard-button.service';

describe('StandardButtonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StandardButtonService = TestBed.get(StandardButtonService);
    expect(service).toBeTruthy();
  });
});
