import { NgModule } from '@angular/core';
import { StandardButtonComponent } from './standard-button.component';

@NgModule({
  declarations: [StandardButtonComponent],
  imports: [
  ],
  exports: [StandardButtonComponent]
})
export class StandardButtonModule { }
