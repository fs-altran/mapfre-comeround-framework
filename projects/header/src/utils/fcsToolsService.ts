import { isObject } from 'util';

export class FcsTools {

  static findObjectInObject(object, name, deepLevel, ignoreLevels, getParent?: any) {
    // tslint:disable-next-line:variable-name
    const _self = FcsTools;
    const item: any = {};
    item.value = undefined;
    if (!ignoreLevels) {
      ignoreLevels = 0;
    }
    if (deepLevel > 0) {
      Object.keys[object].forEach((key) => {
        const value = object[key];
        if (key === name && 0 >= ignoreLevels) {
          item.value = value;
          deepLevel = 0;
        } else {
          if (!item.value && item.value !== '' && item.value !== false && deepLevel > 0 && isObject(value)) {
            item.value = _self.findObjectInObject(value, name, deepLevel - 1, ignoreLevels - 1).value;
            if ((item.value || item.value === '' || item.value === false) && getParent) {
              item.parent = key;
            }
          }
        }
      });
    }
    return item;
  }

  static isEmpty(param) {
    const hasOwnProperty = Object.prototype.hasOwnProperty;
    // null y undefined se consideran vacíos
    if (param == null) { return true; }

    // Se asume que no está vacío si el parametro tiene una propiedad length y es mayor que 0
    if (param.length > 0) { return false; }
    if (param.length === 0) { return true; }

    // En este punto, si el parámetro NO es un objeto, se considera vacío
    // if (typeof param !== "object") return true;
    if (!isObject(param)) { return true; }

    // Comprueba si el objeto tiene propiedades "propias", si no tiene ninguna, se considera vacío
    for (const key in param) {
      if (hasOwnProperty.call(param, key)) { return false; }
    }

    return true;
  }
}
