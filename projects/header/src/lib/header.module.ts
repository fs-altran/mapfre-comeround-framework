import { NgModule } from '@angular/core';
import { HeaderComponent } from './header.component';
import { CommonModule } from '@angular/common';
import { MfcRichedContentComponent } from './mfc-riched-content/mfc-riched-content.component';

@NgModule({
  declarations: [HeaderComponent, MfcRichedContentComponent],
  imports: [
    CommonModule
  ],
  exports: [HeaderComponent]
})
export class HeaderModule { }
