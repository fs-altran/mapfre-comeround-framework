import { Component, OnInit, Input } from '@angular/core';
import { isUndefined } from 'util';

interface MfcRichedContentProperties {
  content?: string;
  visibleOnMobile?: boolean;
  name?: string;
  classname?: string;
}


@Component({
  selector: 'mfc-riched-content',
  templateUrl: './mfc-riched-content.component.html',
  styleUrls: ['./mfc-riched-content.component.scss']
})
export class MfcRichedContentComponent implements OnInit {

  @Input() mfcProperties: MfcRichedContentProperties;
  @Input() parentName: string;

  public componentName = 'mfcRichedContent';
  mobile: any;

  constructor() { }

  ngOnInit() {
    if (!this.mfcProperties) {
      this.mfcProperties = {
        content: 'El descuento es $[0]. $$D[##discount:inTransition##]$$F[##number:2##]',
        visibleOnMobile: true,
        classname: 'none'
      };
    }
    this.initialize();
  }

  initialize() {
    // this.mobile = mfcGlobalData.browser.mobile;
    // this.mgvProp.visibleOnMobile = mfcDom.updateVisibleOnMobile(el, scope.visibleOnMobile);
    // Debido a que no en todos los esquemas se ha incluido (necesario para componentización)
    if (isUndefined(this.mfcProperties.name)) {
      this.mfcProperties.name = this.parentName + '_' + this.componentName;
    }
  }

  outcome(event) {

  }
}
