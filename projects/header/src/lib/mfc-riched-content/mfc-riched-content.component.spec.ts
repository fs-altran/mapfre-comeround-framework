import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MfcRichedContentComponent } from './mfc-riched-content.component';

describe('MfcRichedContentComponent', () => {
  let component: MfcRichedContentComponent;
  let fixture: ComponentFixture<MfcRichedContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MfcRichedContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MfcRichedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
