import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardDateComponent } from './standard-date.component';

describe('StandardDateComponent', () => {
  let component: StandardDateComponent;
  let fixture: ComponentFixture<StandardDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
