import { NgModule } from '@angular/core';
import { StandardDateComponent } from './standard-date.component';

@NgModule({
  declarations: [StandardDateComponent],
  imports: [
  ],
  exports: [StandardDateComponent]
})
export class StandardDateModule { }
