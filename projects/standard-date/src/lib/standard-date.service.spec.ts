import { TestBed } from '@angular/core/testing';

import { StandardDateService } from './standard-date.service';

describe('StandardDateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StandardDateService = TestBed.get(StandardDateService);
    expect(service).toBeTruthy();
  });
});
