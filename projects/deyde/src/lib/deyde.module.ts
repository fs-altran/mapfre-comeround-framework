import { NgModule } from '@angular/core';
import { DeydeComponent } from './deyde.component';

@NgModule({
  declarations: [DeydeComponent],
  imports: [
  ],
  exports: [DeydeComponent]
})
export class DeydeModule { }
