import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeydeComponent } from './deyde.component';

describe('DeydeComponent', () => {
  let component: DeydeComponent;
  let fixture: ComponentFixture<DeydeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeydeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeydeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
