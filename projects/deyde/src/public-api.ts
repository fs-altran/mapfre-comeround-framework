/*
 * Public API Surface of deyde
 */

export * from './lib/deyde.service';
export * from './lib/deyde.component';
export * from './lib/deyde.module';
