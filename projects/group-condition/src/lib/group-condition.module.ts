import { NgModule } from '@angular/core';
import { GroupConditionComponent } from './group-condition.component';

@NgModule({
  declarations: [GroupConditionComponent],
  imports: [
  ],
  exports: [GroupConditionComponent]
})
export class GroupConditionModule { }
