import { TestBed } from '@angular/core/testing';

import { GroupConditionService } from './group-condition.service';

describe('GroupConditionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupConditionService = TestBed.get(GroupConditionService);
    expect(service).toBeTruthy();
  });
});
