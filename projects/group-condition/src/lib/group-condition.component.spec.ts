import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupConditionComponent } from './group-condition.component';

describe('GroupConditionComponent', () => {
  let component: GroupConditionComponent;
  let fixture: ComponentFixture<GroupConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
