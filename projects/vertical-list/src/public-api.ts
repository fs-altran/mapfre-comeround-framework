/*
 * Public API Surface of vertical-list
 */

export * from './lib/vertical-list.service';
export * from './lib/vertical-list.component';
export * from './lib/vertical-list.module';
