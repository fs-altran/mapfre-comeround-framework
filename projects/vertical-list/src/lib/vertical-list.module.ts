import { NgModule } from '@angular/core';
import { VerticalListComponent } from './vertical-list.component';

@NgModule({
  declarations: [VerticalListComponent],
  imports: [
  ],
  exports: [VerticalListComponent]
})
export class VerticalListModule { }
