import { TestBed } from '@angular/core/testing';

import { VerticalListService } from './vertical-list.service';

describe('VerticalListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VerticalListService = TestBed.get(VerticalListService);
    expect(service).toBeTruthy();
  });
});
