import { TestBed } from '@angular/core/testing';

import { NumberFieldService } from './number-field.service';

describe('NumberFieldService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NumberFieldService = TestBed.get(NumberFieldService);
    expect(service).toBeTruthy();
  });
});
