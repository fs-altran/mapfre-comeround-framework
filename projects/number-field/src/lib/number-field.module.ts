import { NgModule } from '@angular/core';
import { NumberFieldComponent } from './number-field.component';

@NgModule({
  declarations: [NumberFieldComponent],
  imports: [
  ],
  exports: [NumberFieldComponent]
})
export class NumberFieldModule { }
