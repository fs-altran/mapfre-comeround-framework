import { Component, OnInit, Input } from '@angular/core';
import { MfcCardProperties } from './card.interface';

@Component({
  selector: 'mfc-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() mfcProperties: MfcCardProperties;

  richedTextDescription;
  richedTextDescriptionPost;
  mobile;
  visibleOnMobile;

  constructor() { }


  ngOnInit() {
    this.initialize();
  }

  initialize() {
    // scope.mobile = mfcGlobalData.browser.mobile;
    // scope.visibleOnMobile = mfcDom.updateVisibleOnMobile(el, scope.visibleOnMobile);
    if (this.mfcProperties.content) {
      this.richedTextDescription = {
        content: this.mfcProperties.content.description,
        visibleOnMobile: true
      };
      this.richedTextDescriptionPost = {
        content: this.mfcProperties.content.descriptionPost,
        visibleOnMobile: true
      };
    }
  }

}
