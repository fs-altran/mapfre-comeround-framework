import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardComponent } from './card.component';
import { MfcRichedContentComponent } from './mfc-riched-content/mfc-riched-content.component';

@NgModule({
  declarations: [CardComponent, MfcRichedContentComponent],
  imports: [
    CommonModule
  ],
  exports: [CardComponent]
})
export class CardModule { }
